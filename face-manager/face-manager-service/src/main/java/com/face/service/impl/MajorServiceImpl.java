package com.face.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.mapper.TbClassMapper;
import com.face.mapper.TbCollegeMapper;
import com.face.mapper.TbMajorMapper;
import com.face.pojo.TbClassExample;
import com.face.pojo.TbCollege;
import com.face.pojo.TbCollegeExample;
import com.face.pojo.TbMajor;
import com.face.pojo.TbMajorExample;
import com.face.pojo.TbMajorExample.Criteria;
import com.face.service.MajorService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class MajorServiceImpl implements MajorService{

	@Autowired
	private TbMajorMapper majorMapper;
	
	@Autowired
	private TbCollegeMapper collegeMapper;
	
	@Autowired
	private TbClassMapper classMapper;
	
	@Override
	public DataGridResult findMajorsByPage(Integer page, Integer rows,Integer type) {
		try {
			TbMajorExample example=new TbMajorExample();
			Criteria criteria = example.createCriteria();
			criteria.andCollegeIdEqualTo(type);
			PageHelper.startPage(page, rows);
			List<TbMajor> list = majorMapper.selectByExample(example);
			if(list!=null&list.size()>0){
				DataGridResult result=new DataGridResult();
				result.setRows(list);
				PageInfo<TbMajor> info=new PageInfo<>(list);
				result.setTotal(info.getTotal());
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public FaceResult add(TbMajor major) {
		try {
			major.setCreateTime(new Date());
			major.setUpdateTime(new Date());
			majorMapper.insert(major);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"添加专业失败");
		}
	}

	@Override
	public FaceResult delete(Integer[] ids) {
		try {
			for(Integer id:ids){
				majorMapper.deleteByPrimaryKey(id);
				
				deleteClassByMajorId(id);
			}
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"删除专业失败");
		}
		
	}
	
	public void deleteClassByMajorId(Integer id){
		try {
			TbClassExample classExample=new TbClassExample();
			classExample.createCriteria().andMajorIdEqualTo(id);
			classMapper.deleteByExample(classExample);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public FaceResult update(TbMajor major) {
		try {
			major.setUpdateTime(new Date());
			majorMapper.updateByPrimaryKeySelective(major);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500, "修改专业失败");
		}
	}

	@Override
	public List<TbMajor> findAll() {
		try {
			TbMajorExample example=new TbMajorExample();
			Criteria criteria = example.createCriteria();
			criteria.andIdIsNotNull();
			List<TbMajor> list = majorMapper.selectByExample(example);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public List<TbMajor> findByCollegeName(String name) {
		try {
			TbCollegeExample collegeExample=new TbCollegeExample();
			com.face.pojo.TbCollegeExample.Criteria criteria2 = collegeExample.createCriteria();
			criteria2.andCollegeNameEqualTo(name);
			List<TbCollege> list2 = collegeMapper.selectByExample(collegeExample);
			TbCollege college = list2.get(0);
			TbMajorExample example=new TbMajorExample();
			Criteria criteria = example.createCriteria();
			criteria.andCollegeIdEqualTo(college.getId());
			List<TbMajor> list = majorMapper.selectByExample(example);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

}

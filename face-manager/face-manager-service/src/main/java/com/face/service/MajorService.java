package com.face.service;

import java.util.List;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbMajor;

public interface MajorService {
	DataGridResult findMajorsByPage(Integer page,Integer rows,Integer type);
	FaceResult add(TbMajor major);
	FaceResult delete(Integer[] ids);
	FaceResult update(TbMajor major);
	List<TbMajor> findAll();
	List<TbMajor> findByCollegeName(String name);
	
}

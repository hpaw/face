package com.face.service;

import java.util.List;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.Query;
import com.face.pojo.TbCheck;
import com.face.pojo.TbStudentCompany;

public interface CheckService {
	
	public DataGridResult getCheckByCollege(Integer page,Integer rows,String collegeName);
	public DataGridResult getCheckByMajor(Integer page,Integer rows,String majorName);
	public DataGridResult getCheckByClass(Integer page,Integer rows,String className,String majorName);
	public DataGridResult getCheckByYear(Integer page,Integer rows,String year);
	FaceResult getCheckListById(Integer id);
	public FaceResult insertStudentCompanyInfo(TbStudentCompany company);
	public FaceResult getCheckTypeById(Integer id);
	DataGridResult list(Integer page, Integer rows, Query query);
	FaceResult check(double lng, double lat, Integer id, String checkInfo, String faceAvatar, String facePhone,
			String faceGroupUid, String faceOpenId);
}

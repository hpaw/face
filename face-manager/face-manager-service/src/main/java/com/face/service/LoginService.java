package com.face.service;

import java.util.List;

import com.face.common.pojo.FaceResult;
import com.face.pojo.SysPermission;
import com.face.pojo.TbTeacher;
import com.face.pojo.TeacherPermissions;

public interface LoginService {
	public TbTeacher login(String teacher);
	public FaceResult updatePass(TbTeacher teacher);
	public List<SysPermission> findPermissionListByTeacherId(Integer id);
	public boolean login(String username,String password);
}

package com.face.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.mapper.TbTeacherMapper;
import com.face.pojo.TbTeacher;
import com.face.pojo.TbTeacherExample;
import com.face.pojo.TbTeacherExample.Criteria;
import com.face.service.TeacherService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class TeacherServiceImpl implements TeacherService{
	
	@Autowired
	private TbTeacherMapper teacherMapper;
	
	@Override
	public FaceResult add(TbTeacher tbTeacher) {
		try {
			tbTeacher.setCreateTime(new Date());
			tbTeacher.setUpdateTime(new Date());
			teacherMapper.insert(tbTeacher);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"添加老师失败!");
		}
	}

	@Override
	public FaceResult checkNumber(String number) {
		try {
			TbTeacherExample example=new TbTeacherExample();
			Criteria criteria = example.createCriteria();
			criteria.andTeacherNumberEqualTo(number);
			List<TbTeacher> list = teacherMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				return FaceResult.ok(false);
			}
			return FaceResult.ok(true);
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500, "服务器出行错误！");
		}
	}

	@Override
	public DataGridResult list(Integer page, Integer rows,String type) {
		try {
			TbTeacherExample example=new TbTeacherExample();
			Criteria criteria = example.createCriteria();
			if(!type.equals("all")){
				criteria.andCollegeIdEqualTo(Integer.valueOf(type));
			}
			PageHelper.startPage(page, rows);
			List<TbTeacher> list = teacherMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				PageInfo<TbTeacher> info=new PageInfo<>(list);
				DataGridResult dataGridResult=new DataGridResult();
				dataGridResult.setRows(list);
				dataGridResult.setTotal(info.getTotal());
				return dataGridResult;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FaceResult delete(Integer[] ids) {
		try {
			for(Integer id:ids){
				teacherMapper.deleteByPrimaryKey(id);
			}
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"删除教师失败,请联系管理员！");
		}
	}

	@Override
	public FaceResult update(TbTeacher teacher) {
		try {
			teacherMapper.updateByPrimaryKeySelective(teacher);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500, "修改教师失败,请联系管理员！");
		}
	}

}

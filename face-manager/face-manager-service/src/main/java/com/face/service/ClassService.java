package com.face.service;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbClass;

public interface ClassService {
	public FaceResult add(TbClass tbClass);
	public boolean check(TbClass tbClass);
	public DataGridResult findClassByPage(Integer id);
	public FaceResult delete(Integer[] ids);
	public FaceResult update(TbClass tbClass);
}

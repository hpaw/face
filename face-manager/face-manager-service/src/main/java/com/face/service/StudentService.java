package com.face.service;

import java.util.List;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbClass;
import com.face.pojo.TbStudent;

public interface StudentService {
	public FaceResult add(TbStudent student);
	public FaceResult checkStuNumber(String number);
	public List<TbClass> ClassInfo(String majorName) ;
	public DataGridResult list(int page,int rows,String type);
	public FaceResult delete(Integer[] ids);
	public FaceResult update(TbStudent student);
	public TbStudent login(String stuNumber,String password);
}

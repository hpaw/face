package com.face.service;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbTeacher;

public interface TeacherService {
	public FaceResult  add(TbTeacher tbTeacher);
	public FaceResult checkNumber(String number);
	public DataGridResult list(Integer page,Integer rows,String type);
	public FaceResult delete(Integer[] ids);
	public FaceResult update(TbTeacher teacher);
	
}

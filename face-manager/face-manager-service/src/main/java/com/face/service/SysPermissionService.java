package com.face.service;

import java.util.List;

import com.face.common.pojo.FaceResult;
import com.face.pojo.MenuAndPermission;
import com.face.pojo.SysPermission;
import com.face.pojo.TeacherPermissions;

public interface SysPermissionService {
	List<MenuAndPermission> getPermissionAndMenu();
	FaceResult insertPermissions(Integer teacherId,Long[] permissions);
	List<SysPermission> getPermissionsByTeacherId(Integer id);
	FaceResult updatePermissions(Integer teacherId,Long[] permissions);
	
}

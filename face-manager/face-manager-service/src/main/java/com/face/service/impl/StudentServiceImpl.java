package com.face.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.mapper.TbClassMapper;
import com.face.mapper.TbMajorMapper;
import com.face.mapper.TbStudentMapper;
import com.face.pojo.TbClass;
import com.face.pojo.TbClassExample;
import com.face.pojo.TbMajor;
import com.face.pojo.TbMajorExample;
import com.face.pojo.TbStudent;
import com.face.pojo.TbStudentExample;
import com.face.pojo.TbStudentExample.Criteria;
import com.face.service.StudentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private TbStudentMapper studentMapper;
	
	@Autowired
	private TbMajorMapper majorMapper;
	
	@Autowired
	private TbClassMapper classMapper;
	
	@Override
	public FaceResult add(TbStudent student) {
		try {
			student.setCreateTime(new Date());
			student.setUpdateTime(new Date());
			student.setPassword(DigestUtils.md5DigestAsHex("111111".getBytes()));
			
			studentMapper.insert(student);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"添加学生成功！");
		}
	}

	@Override
	public FaceResult checkStuNumber(String number) {
		try {
			TbStudentExample example=new TbStudentExample();
			Criteria criteria = example.createCriteria();
			criteria.andStudentNumberEqualTo(number);
			List<TbStudent> list = studentMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				return FaceResult.ok(false);
			}
			return FaceResult.ok(true);
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"服务器出行错误！请联系管理员");
		}
		
	}
	
	public List<TbClass> ClassInfo(String majorName) {
		try {
			TbMajorExample example=new TbMajorExample();
			com.face.pojo.TbMajorExample.Criteria criteria = example.createCriteria();
			criteria.andMajorNameEqualTo(majorName);
			List<TbMajor> list = majorMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				TbMajor major = list.get(0);
				TbClassExample classExample=new TbClassExample();
				com.face.pojo.TbClassExample.Criteria criteria2 = classExample.createCriteria();
				criteria2.andMajorIdEqualTo(major.getId());
				List<TbClass> list2 = classMapper.selectByExample(classExample);
				return list2;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public DataGridResult list(int page, int rows,String type) {
		try {
			TbStudentExample example=new TbStudentExample();
			Criteria criteria = example.createCriteria();
			if(!type.equals("all")){
				String[] strings = type.split(":");
				criteria.andMajorNameEqualTo(strings[0]);
				criteria.andClassNameEqualTo(strings[1]);
			}
			
			PageHelper.startPage(page, rows);
			List<TbStudent> list = studentMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				PageInfo<TbStudent> info=new PageInfo<>(list);
				DataGridResult result=new DataGridResult();
				result.setRows(list);
				result.setTotal(info.getTotal());
				return result;
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public FaceResult delete(Integer[] ids) {
		try {
			for(Integer id:ids){
				studentMapper.deleteByPrimaryKey(id);	
			}
			return FaceResult.ok();	
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"删除学生失败");
		}
		
	}

	@Override
	public FaceResult update(TbStudent student) {
		try {
			student.setUpdateTime(new Date());
			studentMapper.updateByPrimaryKeySelective(student);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"修改学生失败，请联系管理员！");
		}
	}

	@Override
	public TbStudent login(String stuNumber, String password) {
		TbStudentExample example=new TbStudentExample();
		Criteria criteria = example.createCriteria();
		criteria.andStudentNumberEqualTo(stuNumber);
		List<TbStudent> list = studentMapper.selectByExample(example);
		
		if(list!=null&&list.size()>0){
			TbStudent student = list.get(0);
			if(student.getPassword().equals(DigestUtils.md5DigestAsHex(password.getBytes()))){
				
				return student;
			}
		}
		return null;
	}


}

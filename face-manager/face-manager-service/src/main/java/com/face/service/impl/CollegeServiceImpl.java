package com.face.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.common.utils.ExceptionUtil;
import com.face.mapper.TbClassMapper;
import com.face.mapper.TbCollegeMapper;
import com.face.mapper.TbMajorMapper;
import com.face.pojo.TbClassExample;
import com.face.pojo.TbCollege;
import com.face.pojo.TbCollegeExample;
import com.face.pojo.TbCollegeExample.Criteria;
import com.face.pojo.TbMajor;
import com.face.pojo.TbMajorExample;
import com.face.service.CollegeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class CollegeServiceImpl implements CollegeService{
	@Autowired
	private TbCollegeMapper collegeMapper;
	
	@Autowired
	private TbMajorMapper majorMapper;
	
	@Autowired
	private TbClassMapper classMapper;
	
	@Override
	public DataGridResult findCollegesByPage(Integer page,Integer rows) {
		TbCollegeExample example=new TbCollegeExample();
		//分页
		PageHelper.startPage(page, rows);
		
		List<TbCollege> list = collegeMapper.selectByExample(example);
		if(list!=null&&list.size()>0){
			DataGridResult result=new DataGridResult();
			
			PageInfo<TbCollege> info=new PageInfo<>(list);
			
			result.setTotal(info.getTotal());
			result.setRows(list);
			return result;
		}
		return null;
	}

	@Override
	public FaceResult deleteByIds(Integer[] ids) {
		try {
			for(Integer id:ids){
				collegeMapper.deleteByPrimaryKey(id);
				List<Integer> majors = deleteMajorsByCollegeId(id);
				if(null!=majors&&majors.size()>0){
					for(Integer majorId:majors){
						deleteClassByMajorId(majorId);
					}	
				}
			
			}	
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,ExceptionUtil.getStackTrace(e));
		}
	}
	
	public List<Integer> deleteMajorsByCollegeId(Integer id){
		
		List result=null;
		
		try {
			TbMajorExample example=new TbMajorExample();
			com.face.pojo.TbMajorExample.Criteria criteria = example.createCriteria();
			criteria.andCollegeIdEqualTo(id);
			List<TbMajor> list = majorMapper.selectByExample(example);
			if(null!=list&&list.size()>0){
				result=new ArrayList<Integer>();
				for(TbMajor major:list){
					result.add(major.getId());
					majorMapper.deleteByPrimaryKey(major.getId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return result;
		
	}
	
	public void deleteClassByMajorId(Integer id){
		try {
			TbClassExample classExample=new TbClassExample();
			classExample.createCriteria().andMajorIdEqualTo(id);
			classMapper.deleteByExample(classExample);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public FaceResult add(TbCollege college) {
		try {
			college.setCreateTime(new Date());
			college.setUpdateTime(new Date());
			boolean b = this.findCollegeByName(college.getCollegeName());
			if(b){
				collegeMapper.insert(college);
				return FaceResult.ok();
			}
			return FaceResult.build(500,"此院系已存在");
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500, "添加院系失败，请联系相关人员!");
		}
	}

	@Override
	public boolean findCollegeByName(String name) {
		TbCollegeExample example=new TbCollegeExample();
		Criteria criteria = example.createCriteria();
		criteria.andCollegeNameEqualTo(name);
		List<TbCollege> list = collegeMapper.selectByExample(example);
		if(list!=null&&list.size()>0){
			return false;
		}
		return true;
	}

	@Override
	public FaceResult update(TbCollege college) {
		try {
			college.setUpdateTime(new Date());
			collegeMapper.updateByPrimaryKey(college);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500, "修改失败，联系相关人员！");
		}
		
	}

	@Override
	public List<TbCollege> findAll() {
		TbCollegeExample example=new TbCollegeExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIsNotNull();
		List<TbCollege> list = collegeMapper.selectByExample(example);
		
		return list;
	}

	@Override
	public TbCollege findById(Integer id) {
		try {
			TbCollege college = collegeMapper.selectByPrimaryKey(id);
			return college;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

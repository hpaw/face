package com.face.service;

import java.util.List;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbCollege;

public interface CollegeService {
	public DataGridResult findCollegesByPage(Integer page,Integer rows);
	public FaceResult deleteByIds(Integer[] ids);
	public FaceResult add(TbCollege college);
	public boolean findCollegeByName(String name);
	public FaceResult update(TbCollege college);
	public List<TbCollege> findAll();
	public TbCollege findById(Integer id);

}

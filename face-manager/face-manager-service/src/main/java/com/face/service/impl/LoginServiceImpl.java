package com.face.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import com.face.common.pojo.FaceResult;
import com.face.mapper.SysPermissionMapper;
import com.face.mapper.TbTeacherMapper;
import com.face.mapper.TeacherPermissionsMapper;
import com.face.pojo.SysPermission;
import com.face.pojo.TbTeacher;
import com.face.pojo.TbTeacherExample;
import com.face.pojo.TbTeacherExample.Criteria;
import com.face.pojo.TeacherPermissions;
import com.face.pojo.TeacherPermissionsExample;
import com.face.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	private TbTeacherMapper teacherMapper;
	
	@Autowired
	private TeacherPermissionsMapper permissionsMapper;
	
	@Autowired
	private SysPermissionMapper sysPermissionMapper;
	
	@Override
	public TbTeacher login(String teacher) {
		try {
			TbTeacherExample example=new TbTeacherExample();
			Criteria criteria = example.createCriteria();
			criteria.andTeacherNumberEqualTo(teacher);
			List<TbTeacher> list = teacherMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
					return list.get(0);
			
			}
			return null;	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FaceResult updatePass(TbTeacher tbTeacher) {
		try {
			teacherMapper.updateByPrimaryKeySelective(tbTeacher);
			return FaceResult.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return FaceResult.build(500,"修改密码失败！请联系管理员");
		}

	}
	
	@Override
	public List<SysPermission> findPermissionListByTeacherId(Integer id){
		List<SysPermission> list2=null;
		try {
			TeacherPermissionsExample example=new TeacherPermissionsExample();
			com.face.pojo.TeacherPermissionsExample.Criteria criteria = example.createCriteria();
			criteria.andTeacherIdEqualTo(id);
			List<TeacherPermissions> list = permissionsMapper.selectByExample(example);
			list2 = findPermissionsByTeacherId(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return list2;
	}

	public List<SysPermission> findPermissionsByTeacherId(List<TeacherPermissions> list) {
		List<SysPermission> lists=new ArrayList<>();
		
		try {
			for(TeacherPermissions permissions:list){
				SysPermission permission = sysPermissionMapper.selectByPrimaryKey(permissions.getSysPermissionId());
				lists.add(permission);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return lists;
	}

	@Override
	public boolean login(String username, String password) {
		boolean result=false;
		try {
			TbTeacherExample example=new TbTeacherExample();
			Criteria criteria = example.createCriteria();
			criteria.andTeacherNumberEqualTo(username);
			List<TbTeacher> list = teacherMapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				TbTeacher teacher = list.get(0);
				if(teacher.getPassword().equals(password)){
					result=true;
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}
	

}

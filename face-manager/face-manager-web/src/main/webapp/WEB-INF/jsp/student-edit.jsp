<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>修改学生</title>
</head>
<body>
	<center>
		<form id="ff" method="post">
			<div>
				<label for="id">编号:</label> <input type="text" name="id"
					readonly="readonly" />
			</div>
			<br />
			<div>
				<label for="studentNumber">学号:</label> <input id="studentNumber"
					type="text" name="studentNumber" readonly="readonly" />
			</div>
			<br> <br> <br>
			<div>
				<label for="studentName">姓名:</label> <input type="text"
					name="studentName" />
			</div>
			<br> <br> <br>
			<div>
				<label for="collegeName">选择院系:</label> <select id="ss"
					class="easyui-combobox" name="collegeName" panelHeight="auto"
					style="width: 200px;">
					<c:forEach items="${list}" var="college">
						<option value="${college.collegeName}">${college.collegeName}</option>
					</c:forEach>
				</select>

			</div>
			<br> <br> <br>

			<div>
				<label for="majorName">选择专业:</label> <select id="mm"
					class="easyui-combobox" name="majorName" panelHeight="auto"
					style="width: 200px;">

				</select>

			</div>

			<br> <br> <br>
			<div>
				<label for="className">所在班级:</label> <select id="nn"
					class="easyui-combobox" name="className" panelHeight="auto"
					style="width: 200px;">

				</select>
			</div>
			<br> <br>
			<div>
				<input id="btn" type="button" value="提交" />
			</div>
		</form>
	</center>
	<script type="text/javascript">
		$(function() {
		
			var win = parent.$("iframe[title='学生信息管理']").get(0).contentWindow;//返回ifram页面文档（window)

			var row = win.$('#dg').datagrid('getSelected');
			$('#ff').form('load', {
				id : row.id,
				studentNumber:row.studentNumber,
				studentName : row.studentName,
				collegeName:row.collegeName,
				majorName:row.majorName,
				className:row.className
				
			});

			$("[name='studentName']").validatebox({
				required : true,
				missingMessage : '请填写学生名！'
			});
			$("[name='studentNumber']").validatebox({
				required : true,
				missingMessage : '请填写学生号！'
			});
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/student/update',
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("修改学生成功");
								parent.$("#win").window("close");
								win.$("#dg").datagrid("reload");	
							} else {
								alert(data.msg);
							}
						}
					});

				}
			});
			$('#ss').combobox({
				onChange : function(newValue, oldValue) {
					$.post("/student/data", {
						"name" : newValue
					}, function(data) {

						var dd = [];
						var d = eval(data);
						$.each(d, function(n, value) {
							dd.push({
								"text" : value.majorName,
								"id" : n,
								"value" : value.majorName
							});
						});

						$("#mm").combobox("loadData", dd);

					});

				}
			});

			$('#mm').combobox({
				onChange : function(newValue, oldValue) {
					$.post("/student/classInfo", {
						"name" : newValue
					}, function(data) {

						var dd = [];
						var d = eval(data);
						$.each(d, function(n, value) {
							dd.push({
								"text" : value.className,
								"id" : n,
								"value" : value.className
							});
						});

						$("#nn").combobox("loadData", dd);

					});

				}
			});

		});
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>院系信息列表</title>
<style type="text/css">
.searchbox {
	margin: -3
}
</style>

<script type="text/javascript">

	 function getSelectionsIds(){
	    	var list = $("#dg");
	    	var sels = list.datagrid("getSelections");
	    	var ids = [];
	    	for(var i in sels){
	    		ids.push(sels[i].id);
	    	}
	    	ids = ids.join(",");
	    	return ids;
	    }
	    
	$(function(){
			$('#dg').datagrid({    
			    url:'/college/list', 
			    fitColumns:true,
			    autoRowHeight:false,
			    nowrap:true,
			    idField:'id',
			    rownumbers:true,
			    pagination:true,
			    pageSize:5,
			    pageList:[3,5,10,20],			    
			   toolbar:[{
					iconCls: 'icon-remove',
					text:'删除院系',
					handler: function(){
						var ids = getSelectionsIds();
			        	if(ids.length == 0){
			        		$.messager.alert('提示','未选中院系!');
			        		return ;
			        	}
			        	$.messager.confirm('确认','确定删除ID为 '+ids+' 的院系吗？',function(r){
			        	    if (r){
			        	    	var params = {"ids":ids};
			                	$.post("/college/delete",params, function(data){
			                		var data = eval('(' + data + ')');
			                	
			            			if(data.status == 200){
			            				$.messager.alert('提示','删除院系成功!',undefined,function(){
			            					$("#dg").datagrid("reload");
			            				});
			            			}
			            		});
			        	   	 }
			        	  });
			        	 }
			        	
				},{
					iconCls: 'icon-edit',
					text:'修改院系',
					handler: function(){
						var array=$('#dg').datagrid('getSelections');
						if(array.length!=1){
							alert('请选择一行,进行院系修改');
							return false;
						}
						parent.$("#win").window({
							title:'修改院系',
							width:600,
							height:600,
							modal:true,
							maximizable:false,
							content:"<iframe src='/college/updateView' height='100%' width='100%' frameborder='0px' ></iframe>"
						});
					}
					
				}],
				columns : [ [
				{
					checkbox:true,
					field:""
				}, {
				field : 'id',
				title : '编号'
			}, {
				field : 'collegeName',
				title : '学院名',
				width : 100
			}, {   
				field : 'createTime',
				title : '创建时间',
				width : 100,
				formatter:face.formatDate
			}, {
				field : 'updateTime',
				title : '修改时间',
				width : 100,
				formatter:face.formatDate
			}] ]
			 
		});

	});
</script>
</head>

<body>
	<table id="dg"></table>
</body>
</html>
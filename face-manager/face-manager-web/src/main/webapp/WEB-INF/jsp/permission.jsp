<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<form id="ff"  method="post">
<input type="hidden" name="teacherId" value="${id}">
<table class="easyui-datagrid" data-options="fitColumns:true,singleSelect:true">   
    <thead>   
        <tr>   
            <th data-options="field:'code',width:100">菜单</th>   
            <th data-options="field:'name',width:100">权限</th>   
        </tr>   
    </thead>   
    <tbody>   
    <c:forEach items="${list}" var="menuAndPermission">
     	<tr>   
            <td>${menuAndPermission.menu.name}</td>
            <td>
            <c:forEach items="${menuAndPermission.permissions}" var="permission">
            
            <input type="checkbox" name="permissions" value="${permission.id}"> ${permission.name} &nbsp;
            </c:forEach> 
            </td>  
            
        </tr>  
    </c:forEach>
           
    </tbody>   
</table>  <br/>
<input type="button" id="btn" value="分配">
</form>
</body>
<script type="text/javascript">
$("#btn").click(function() {
	$('#ff').form('submit', {
						url : '/permission/addPermission',
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("分配权限成功");
								$("#ff").form('clear');
								parent.$("#win").window("close");
							} else {
								alert(data.msg);
							}
						}
	});
});
</script>
</html>
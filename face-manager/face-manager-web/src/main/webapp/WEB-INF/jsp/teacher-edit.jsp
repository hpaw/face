<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>修改教师</title>
</head>
<body>
	<center>
		<form id="ff" method="post">
			<div>
				<label for="id">编号:</label> <input type="text" name="id"
					readonly="readonly" />
			</div>
			<br />
			<div>
				<label for="teacherNumber">教师号:</label> <input id="teacherNumber"
					type="text" name="teacherNumber" readonly="readonly" />
			</div>
			<br> <br> <br>
			<div>
				<label for="teacherName">姓名:</label> <input type="text"
					name="teacherName" />
			</div>
			<br> <br> <br>
			<div>
				<label for="collegeId">选择院系:</label> <select class="easyui-combobox"
					name="collegeId" panelHeight="auto" style="width: 200px;">
					<option></option>
					<c:forEach items="${list}" var="college">
						<option value="${college.id}">${college.collegeName}</option>
					</c:forEach>
				</select>

			</div>
			<br> <br> <br>
			<div>
				<label for="teacherType">老师类别:</label> <select
					class="easyui-combobox" name="teacherType" panelHeight="auto"
					style="width: 200px;">
					<option value="普通教师">普通教师</option>
					<option value="管理员">管理员</option>
				</select>
			</div>

			<br> <br>
			<div>
				<input id="btn" type="button" value="提交" />
			</div>
		</form>
	</center>
	<script type="text/javascript">
		$(function() {
		
			var win = parent.$("iframe[title='教师信息管理']").get(0).contentWindow;//返回ifram页面文档（window)

			var row = win.$('#dg').datagrid('getSelected');
			$('#ff').form('load', {
				id : row.id,
				teacherNumber:row.teacherNumber,
				teacherName : row.teacherName,
				teacherType:row.teacherType
				
			});

			$("[name='teacherName']").validatebox({
				required : true,
				missingMessage : '请填写教师名！'
			});
			$("[name='teacherNumber']").validatebox({
				required : true,
				missingMessage : '请填写教师号！'
			});
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/teacher/update',
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("修改教师成功");
								parent.$("#win").window("close");
								win.$("#dg").datagrid("reload");	
							} else {
								alert(data.msg);
							}
						}
					});

				}
			});
		});
	</script>
</body>
</html>
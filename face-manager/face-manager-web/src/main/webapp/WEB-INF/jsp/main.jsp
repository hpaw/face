<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<title>考勤后台管理系统</title>
<script type="text/javascript">
	$(function(){
		$("a[title]").click(function(){
			var text=this.href;
			if($("#tt").tabs("exists",this.title)){
				$("#tt").tabs('select',this.title);
			}else{
				$("#tt").tabs('add',{
					title:this.title,
					closable:true,
					content:"<iframe src='"+text+"' title='"+this.title+"' height='100%' width='100%' frameborder='0px' ></iframe>"
				});
			}
			return false;	
		});
	});

</script>

</head>
<body class="easyui-layout">
	<div region="north" split="true" border="false"
		style="overflow: hidden; height: 30px; background: url(/images/layout-browser-hd-bg.gif) #7f99be repeat-x center 50%; line-height: 20px; color: #fff; font-family: Verdana, 微软雅黑, 黑体">
		<span style="float: right; padding-right: 20px;" class="head">欢迎,${sessionScope.teacher.teacherName}
			<a href="/updatePasswordView" title="修改密码" style="color: white;"
			id="editpass">修改密码</a> <a href="/logout" style="color: white;"
			id="loginOut">安全退出</a>
		</span> <span style="padding-left: 10px; font-size: 16px;"><img
			src="/images/blocks.gif" width="20" height="20" align="absmiddle" />
			考勤后台管理系统</span>
	</div>
	<div
		data-options="region:'west',title:'导航菜单',split:false,collapsible:false"
		style="width: 150px;">
		<div id="aa" class="easyui-accordion"
			style="width: 300px; height: 200px;"
			data-options="fit:true,selected:-1">
			
			<div title="权限管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/permission/permissionAndMenu"
						title="权限列表"
						style="text-decoration: none; display: block; font-weigth: bold;">权限列表</a></li>
				</ul>
			</div>
			
			
			<div title="院系管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/college/collegeView"
						title="院系信息列表"
						style="text-decoration: none; display: block; font-weigth: bold;">院系信息列表</a></li>
					<li style="padding: 6px;"><a href="/college/add" title="添加院系"
						style="text-decoration: none; display: block; font-weigth: bold;">添加院系</a></li>
				</ul>
			</div>

			<div title="专业管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/major/majorView"
						title="专业信息列表"
						style="text-decoration: none; display: block; font-weigth: bold;">专业信息列表</a></li>
					<li style="padding: 6px;"><a href="/major/addView"
						title="添加专业"
						style="text-decoration: none; display: block; font-weigth: bold;">添加专业</a></li>
				</ul>
			</div>

			<div title="班级管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/class/listView"
						title="班级信息列表"
						style="text-decoration: none; display: block; font-weigth: bold;">班级信息列表</a></li>
					<li style="padding: 6px;"><a href="/class/addView"
						title="添加班级"
						style="text-decoration: none; display: block; font-weigth: bold;">添加班级</a></li>
				</ul>
			</div>

			<div title="教师管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/teacher/listView"
						title="教师信息管理"
						style="text-decoration: none; display: block; font-weigth: bold;">教师信息管理</a></li>
					<li style="padding: 6px;"><a href="/teacher/addView"
						title="添加教师"
						style="text-decoration: none; display: block; font-weigth: bold;">添加教师</a></li>
				</ul>
			</div>

			<div title="学生管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/student/listView"
						title="学生信息管理"
						style="text-decoration: none; display: block; font-weigth: bold;">学生信息管理</a></li>
					<li style="padding: 6px;"><a href="/student/addView"
						title="添加学生"
						style="text-decoration: none; display: block; font-weigth: bold;">添加学生</a></li>
				</ul>
			</div>

			<div title="考勤管理">
				<ul style="list-style: none; padding: 0px; margin: 0px;">
					<li style="padding: 6px;"><a href="/check/query" title="考勤查询"
						style="text-decoration: none; display: block; font-weigth: bold;">考勤查询</a></li>

				</ul>
			</div>

		</div>
	</div>

	<div data-options="region:'center',title:'主体内容',collapsible:false">
		<div id="tt" class="easyui-tabs" style="width: 500px; height: 250px;"
			data-options="fit:true">
		</div>
	
	</div>
	<div region="south" split="false"
		style="height: 30px; background: #D2E0F2; text-align: center; color: #15428B; margin: 0px; padding: 0px; line-height: 23px; font-weight: bold;">
		<div class="footer">
			<span style="font-family: arial;">内江师范学院 版权所有 &copy; 2017
				NJTC.EDU.CN</span>
		</div>
	</div>
	<div id="win"></div>

</body>
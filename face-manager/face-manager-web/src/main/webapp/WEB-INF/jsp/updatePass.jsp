<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>修改密码</title>
</head>
<body>
	<center>
		<form id="ff" method="post">



			<div>
				<label for="password">新密码:</label> <input type="password"
					id="password" name="password" />
			</div>
			<br>

			<div>
				<label for="pwd">再次输入:</label> <input type="password" id="pwd"
					name="pwd" />
			</div>
			<br>


			<div>
				<input id="btn" type="button" value="提交" />
			</div>
		</form>
	</center>
	<script type="text/javascript">
		$(function() {
	
			$("[name='password']").validatebox({
				required : true,
				missingMessage : '请填写密码！'
			});
			
			$("[name='pwd']").validatebox({
				required : true,
				missingMessage : '请再次填写密码！'
			});
			
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/updatePassword',
						onSubmit : function() {
							
							var password=$("#password").val();
							var pwd=$("#pwd").val();
							if(pwd==password){
								return true;
							}
							alert("两次密码不一致，请重新输入！");
							return false;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("修改密码成功");
								this.window('close'); 

							} else {
								alert(data.msg);
							}
						}
					});

				}

			});

		});
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<link href="/js/kindeditor-4.1.10/themes/default/default.css"
	type="text/css" rel="stylesheet">
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>修改院系</title>
</head>
<body>
	<form id="ff" method="post">

		<div>
			<label for="id">院系编号:</label> <input type="text" name="id"
				readonly="readonly" />
		</div>
		<br />

		<div>
			<label for="collegeName">院系名:</label> <input type="text"
				name="collegeName" />
		</div>
		<br />

		<div>
			<input id="btn" type="button" value="提交" />
		</div>
	</form>
	<script type="text/javascript">
		$(function() {
			var win = parent.$("iframe[title='院系信息列表']").get(0).contentWindow;//返回ifram页面文档（window)
			
			var row=win.$('#dg').datagrid('getSelected');
			$('#ff').form('load',{
				id:row.id,
				collegeName:row.collegeName
			});

			$("[name='collegeName']").validatebox({
				required : true,
				missingMessage : '请填写院系！'
			});
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/college/update',
						onSubmit : function() {
							return true;
						},
						success : function(data) {							
								//可以定义为对应消息框
								 var data = eval('(' + data + ')');  
							     if (data.status==200){    
							          alert("修改院系成功");    
							      }  else{
							      	 alert("修改院系失败");    
							      }
								parent.$("#win").window("close");
								win.$("#dg").datagrid("reload");							
						}
					});

				}

			});

		});
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>  


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>班级信息列表</title>
<style type="text/css">
.searchbox {
	margin: -3
}
</style>

<script type="text/javascript">

	 function getSelectionsIds(){
	    	var list = $("#dg");
	    	var sels = list.datagrid("getSelections");
	    	var ids = [];
	    	for(var i in sels){
	    		ids.push(sels[i].id);
	    	}
	    	ids = ids.join(",");
	    	return ids;
	    }
	    
	$(function(){
			$('#dg').datagrid({    
			    url:'/class/list', 
			    fitColumns:true,
			    autoRowHeight:false,
			    nowrap:true,
			    idField:'id',
			    rownumbers:true,
			    pagination:true,
			    pageSize:5,
			    pageList:[3,5,10,20],
			    queryParams: {
					type:'1'
				},			    
			   toolbar:'#tb',
				columns : [ [
				{
					checkbox:true,
					field:""
				}, {
				field : 'id',
				title : '编号'
			}, {
				field : 'classNumber',
				title : '班级号',
				width : 100
			}, {
				field : 'className',
				title : '班级名',
				width : 100
			}, {
				field : 'classAge',
				title : '所在年级',
				width : 100
			}, {   
				field : 'createTime',
				title : '创建时间',
				width : 100,
				formatter:face.formatDate
			}, {
				field : 'updateTime',
				title : '修改时间',
				width : 100,
				formatter:face.formatDate
			}] ]
			 
		});
		$('#ss').combobox({ 
			onChange:function(newValue, oldValue){ 
				$('#dg').datagrid('load',{
					type: newValue
				});
			}
		}); 

	});
	
	function update(){
		var array=$('#dg').datagrid('getSelections');
							if(array.length!=1){
								alert('请选择一行,进行班级修改');
								return false;
							}
							parent.$("#win").window({
								title:'修改班级',
								width:600,
								height:600,
								modal:true,
								maximizable:false,
								content:"<iframe src='/class/updateView' height='100%' width='100%' frameborder='0px' ></iframe>"
							});
	}
	
	function deleteMajor(){
		var ids = getSelectionsIds();
			        	if(ids.length == 0){
			        		$.messager.alert('提示','未选中班级!');
			        		return ;
			        	}
			        	$.messager.confirm('确认','确定删除ID为 '+ids+' 的班级吗？',function(r){
			        	    if (r){
			        	    	var params = {"ids":ids};
			                	$.post("/class/delete",params, function(data){
			                		var data = eval('(' + data + ')');
			            			if(data.status == 200){
			            				$.messager.alert('提示','删除班级成功!',undefined,function(){
			            					$("#dg").datagrid("reload");
			            				});
			            			}
			            		});
			        	   	 }
			        	  });
			        }
</script>
</head>

<body>
	<table id="dg"></table>
	<div id="tb" style="padding: 5px">

		<div>
			查询： <select id="ss" name="type" class="easyui-combobox"
				panelHeight="auto" style="width: 100px">
				<option></option>

				<c:forEach items="${list}" var="college">
					<option value="${college.id}">${college.collegeName}</option>

				</c:forEach>
			</select> <a href="#" class="easyui-linkbutton" iconCls="icon-edit"
				plain="true" onclick="update();">修改班级</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-remove" plain="true"
				onclick="deleteMajor();">删除班级</a>

		</div>
	</div>
</body>
</html>
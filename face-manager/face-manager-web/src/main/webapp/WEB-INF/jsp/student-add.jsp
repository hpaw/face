<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>添加学生</title>
</head>
<body>
	<center>
		<form id="ff" method="post">
			<div>
				<label for="studentNumber">学号:</label> <input id="studentNumber"
					type="text" name="studentNumber" />
			</div>
			<br> <br> <br>
			<div>
				<label for="studentName">姓名:</label> <input type="text"
					name="studentName" />
			</div>
			<br> <br> <br>
			<div>
				<label for="collegeName">选择院系:</label> <select id="ss"
					class="easyui-combobox" name="collegeName" panelHeight="auto"
					style="width: 200px;">
					<option></option>
					<c:forEach items="${list}" var="college">
						<option value="${college.collegeName}">${college.collegeName}</option>
					</c:forEach>
				</select>

			</div>
			<br> <br> <br>

			<div>
				<label for="majorName">选择专业:</label> <select id="mm"
					class="easyui-combobox" name="majorName" panelHeight="auto"
					style="width: 200px;">

				</select>

			</div>

			<br> <br> <br>
			<div>
				<label for="className">所在班级:</label> <select id="nn"
					class="easyui-combobox" name="className" panelHeight="auto"
					style="width: 200px;">

				</select>
			</div>
			<br> <br>
			<div>
				<input id="btn" type="button" value="提交" />
			</div>
		</form>
	</center>
	<script type="text/javascript">
		$(function() {

			$("#studentNumber").blur(function() {
				$.post("/student/checkStuNumber", {
					"number" : $("#studentNumber").val()
				}, function(data) {
					if (data.data == false) {
						alert("此学号已存在，请确认！");
						$("#studentNumber").val("");
						return;
					}
				})
			});
			
			$("[name='studentName']").validatebox({
				required : true,
				missingMessage : '请填写学生名！'
			});
			$("[name='studentNumber']").validatebox({
				required : true,
				missingMessage : '请填写学生号！'
			});
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/student/add',
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("添加数据成功");
								$("#ff").form('clear');
							} else {
								alert(data.msg);
							}
						}
					});

				}
			});
			$('#ss').combobox({
				onChange : function(newValue, oldValue) {
					$.post("/student/data", {
						"name" : newValue
					}, function(data) {

						var dd = [];
						var d = eval(data);
						$.each(d, function(n, value) {
							dd.push({
								"text" : value.majorName,
								"id" : n,
								"value" : value.majorName
							});
						});

						$("#mm").combobox("loadData", dd);

					});

				}
			});
			
				$('#mm').combobox({
				onChange : function(newValue, oldValue) {
					$.post("/student/classInfo", {
						"name" : newValue
					}, function(data) {

						var dd = [];
						var d = eval(data);
						$.each(d, function(n, value) {
							dd.push({
								"text" : value.className,
								"id" : n,
								"value" : value.className
							});
						});

						$("#nn").combobox("loadData", dd);

					});

				}
			});

		});
	</script>
</body>
</html>
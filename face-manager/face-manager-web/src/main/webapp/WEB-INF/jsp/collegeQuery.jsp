<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>考勤信息列表</title>
<style type="text/css">
.searchbox {
	margin: -3
}
</style>

<script type="text/javascript">
	$(function() {
		$('#dg').datagrid({
			url : '/check/list',
			fitColumns : true,
			autoRowHeight : false,
			nowrap : true,
			idField : 'id',
			rownumbers : true,
			pagination : true,
			pageSize : 5,
			pageList : [ 3, 5, 10, 20 ],
			toolbar : '#tb',
			columns : [ [ {
				checkbox : true,
				field : ""
			}, {
				field : 'id',
				title : '编号'
			}, {
				field : 'studentName',
				title : '学生名',
				width : 100
			}, {
				field : 'collegeName',
				title : '所在院系',
				width : 100
			}, {
				field : 'majorName',
				title : '所在专业',
				width : 100
			}, {
				field : 'className',
				title : '所在班级',
				width : 100
			}, {
				field : 'checkType',
				title : '签到状态',
				width : 100
			}, {
				field : 'checkTime',
				title : '签到时间',
				width : 100,
				formatter : face.formatDateTime
			}, {
				field : 'checkAddress',
				title : '签到地址',
				width : 100
			}, {
				field : 'checkYear',
				title : '签到年份',
				width : 100
			} ] ]

		});
		$('#ss').combobox({
			onChange : function(newValue, oldValue) {
				$.post("/student/data", {
					"name" : newValue
				}, function(data) {

					var dd = [];
					var d = eval(data);
					$.each(d, function(n, value) {
						dd.push({
							"text" : value.majorName,
							"id" : n,
							"value" : value.majorName
						});
					});

					$("#mm").combobox("loadData", dd);

				});

			}
		});
		$('#mm').combobox({
			onChange : function(newValue, oldValue) {

				$.post("/student/classInfo", {
					"name" : newValue
				}, function(data) {

					var dd = [];
					var d = eval(data);
					$.each(d, function(n, value) {
						dd.push({
							"text" : value.className,
							"id" : n,
							"value" : value.className
						});
					});

					$("#cc").combobox("loadData", dd);

				});

			}
		});

		$('#btn').bind(
				'click',
				function() {
					var year = $('#year').numberspinner('getValue');
					var collegeName = $('#ss').combobox('getValue');
					var majorName = $('#mm').combobox('getValue');
					var className = $('#cc').combobox('getValue');
					if (year == '' && collegeName == '' && majorName == ''
							&& className == '') {
						$.messager.alert('提示', '查询条件不能为空');
						return;
					}
					var params = {
						year : year,
						collegeName : collegeName,
						className : className,
						majorName : majorName
					};
					$('#dg').datagrid('load', params);
				});

	});
</script>
</head>

<body>
	<table id="dg"></table>
	<form id="ff">
		<div id="tb" style="padding: 5px">
			<div>
				年份： <input id="year" name="checkYear" class="easyui-numberspinner"
					style="width: 80px;" required="required"
					data-options="min:2017,max:9999,editable:false">

				<!-- <option value="2018">2018</option> -->
				院系： <select id="ss" name="collegeName" class="easyui-combobox"
					panelHeight="auto" style="width: 100px">
					<option></option>

					<c:forEach items="${list}" var="college">
						<option value="${college.collegeName}">${college.collegeName}</option>
					</c:forEach>
				</select> 专业:<select id="mm" name="majorName" class="easyui-combobox"
					panelHeight="auto" style="width: 100px">
					<option></option>


				</select> 班级：<select id="cc" name="className" class="easyui-combobox"
					panelHeight="auto" style="width: 100px">
					<option></option>
				</select> <a id="btn" href="#" class="easyui-linkbutton"
					data-options="iconCls:'icon-search'">查询</a>
			</div>

		</div>
	</form>
</body>
</html>
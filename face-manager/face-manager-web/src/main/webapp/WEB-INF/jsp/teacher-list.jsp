<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>教师信息列表</title>
<style type="text/css">
.searchbox {
	margin: -3
}
</style>

<script type="text/javascript">
	function getSelectionsIds() {
		var list = $("#dg");
		var sels = list.datagrid("getSelections");
		var ids = [];
		for ( var i in sels) {
			ids.push(sels[i].id);
		}
		ids = ids.join(",");
		return ids;
	}

	$(function() {
		$('#dg').datagrid({
			url : '/teacher/list',
			fitColumns : true,
			autoRowHeight : false,
			nowrap : true,
			idField : 'id',
			rownumbers : true,
			pagination : true,
			pageSize : 5,
			pageList : [ 3, 5, 10, 20 ],
			queryParams : {
				type : 'all'
			},
			toolbar : '#tb',
			columns : [ [ {
				checkbox : true,
				field : ""
			}, {
				field : 'id',
				title : '编号'
			}, {
				field : 'teacherNumber',
				title : '教师号',
				width : 100
			}, {
				field : 'teacherName',
				title : '教师名',
				width : 100
			}, {
				field : 'teacherType',
				title : '教师类别',
				width : 100
			},{
				field : 'createTime',
				title : '创建时间',
				width : 100,
				formatter : face.formatDate
			}, {
				field : 'updateTime',
				title : '修改时间',
				width : 100,
				formatter : face.formatDate
			}, {
				field : 'opt',
				title : '权限设置',
				width : 100,
				formatter: function (value,row,index) {
                            return '<a href="#" style="text-decoration:none" onclick="addPermission('+row.id+')" > <span style="color:#34537E">分配权限</span></a>　　<a href="#" onclick="editPermission('+row.id+')" style="text-decoration:none"> <span style="color:#34537E">修改权限</span></a>';
                        }
			} ] ]

		});

		$('#ss').combobox({
			onChange : function(newValue, oldValue) {
				$('#dg').datagrid('load', {
					type : newValue
				});
			}
		});

	});

	function update() {
		var array = $('#dg').datagrid('getSelections');
		if (array.length != 1) {
			alert('请选择一行,进行教师修改');
			return false;
		}
		parent
				.$("#win")
				.window(
						{
							title : '修改教师',
							width : 600,
							height : 600,
							modal : true,
							maximizable : false,
							content : "<iframe src='/teacher/updateView' height='100%' width='100%' frameborder='0px' ></iframe>"
						});
	}
	
	function addPermission(id) {
		parent
				.$("#win")
				.window(
						{
							title : '分配权限',
							width : 800,
							height : 600,
							modal : true,
							maximizable : false,
							content : "<iframe src='/permission/addPermissionView/"+id+"' height='100%' width='100%' frameborder='0px' ></iframe>"
						});
	}
	
	function editPermission(id) {
		parent
				.$("#win")
				.window(
						{
							title : '修改权限',
							width : 800,
							height : 600,
							modal : true,
							maximizable : false,
							content : "<iframe src='/permission/editPermissionView/"+id+"' height='100%' width='100%' frameborder='0px' ></iframe>"
						});
	}

	function deleteMajor() {
		var ids = getSelectionsIds();
		if (ids.length == 0) {
			$.messager.alert('提示', '未选中教师!');
			return;
		}
		$.messager.confirm('确认', '确定删除ID为 ' + ids + ' 的教师吗？', function(r) {
			if (r) {
				var params = {
					"ids" : ids
				};
				$.post("/teacher/delete", params, function(data) {
					var data = eval('(' + data + ')');
					if (data.status == 200) {
						$.messager.alert('提示', '删除教师成功!', undefined,
								function() {
									$("#dg").datagrid("reload");
						});
					}else{
						$.messager.alert('提示',data.msg);
					}
				});
			}
		});
	}
</script>
</head>

<body>
	<table id="dg"></table>
	<div id="tb" style="padding: 5px">

		<div>
			院系： <select id="ss" name="type" class="easyui-combobox"
				panelHeight="auto" style="width: 100px">
				<option></option>
				<c:forEach items="${list}" var="college">
					<option value="${college.id}">${college.collegeName}</option>
				</c:forEach>
			</select> <a href="#" class="easyui-linkbutton" iconCls="icon-edit"
				plain="true" onclick="update();">修改教师</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-remove" plain="true"
				onclick="deleteMajor();">删除教师</a>

		</div>
	</div>
</body>
</html>
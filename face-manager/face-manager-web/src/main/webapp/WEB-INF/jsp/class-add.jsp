<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="/js/jquery-easyui-1.4.1/themes/icon.css" />
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/js/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<title>添加班级</title>
</head>
<body>
	<center>
		<form id="ff" method="post">

			<div>
				<label for="collegeId">选择院系:</label> <select id="ss"
					class="easyui-combobox" name="collegeId" style="width: 200px;">
					<option></option>
					<c:forEach items="${colleges}" var="college">
						<option value="${college.collegeName}">${college.collegeName}</option>
					</c:forEach>
				</select>
			</div>
			<br>

			<div>
				<label for="majorId">选择专业:</label> <select id="mm"
					class="easyui-combobox" name="majorId" style="width: 200px;">

				</select>
			</div>
			<br>

			<div>
				<label for="classNumber">选择班级号:</label> <input name="classNumber"
					class="easyui-numberspinner" style="width: 80px;"
					required="required" data-options="min:1,max:15,editable:false">
			</div>
			<br>

			<div>
				<label for="classAge">所在年级:</label> <input name="classAge"
					class="easyui-numberspinner" style="width: 80px;"
					required="required" data-options="min:2014,max:9999,editable:false">
			</div>
			<br>

			<div>
				<input id="btn" type="button" value="提交" />
			</div>
		</form>
	</center>
	<script type="text/javascript">
		$(function() {
		
		$('#ss').combobox({
				onChange : function(newValue, oldValue) {
					$.post("/student/data", {
						"name" : newValue
					}, function(data) {

						var dd = [];
						var d = eval(data);
						$.each(d, function(n, value) {
							dd.push({
								"text" : value.majorName,
								"id" : n,
								"value" : value.id
							});
						});

						$("#mm").combobox("loadData", dd);

					});

				}
			});
			
			$("[name='majorId']").validatebox({
				required : true,
				missingMessage : '请填写专业！'
			});
			//禁用验证
			$("#ff").form("disableValidation");

			$("#btn").click(function() {
				$("#ff").form("enableValidation");
				if ($("#ff").form("validate")) {
					$('#ff').form('submit', {
						url : '/class/add',
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							//可以定义为对应消息框
							var data = eval('(' + data + ')');
							if (data.status == 200) {
								alert("添加班级成功");
								$("#ff").form('clear');

							} else {
								alert(data.msg);
							}
						}
					});

				}

			});

		});
	</script>
</body>
</html>
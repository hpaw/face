package com.face.client;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.FaceResult;
import com.face.pojo.TbCheck;
import com.face.pojo.TbStudentCompany;
import com.face.service.CheckService;

@Controller
@RequestMapping("/stuCheck")
public class StudentCheckController {
	@Autowired
	private CheckService checkService;
	
	
	/*
	 * 获取签到信息列表
	 * id 用户id
	 */
	@RequestMapping(value="/checkList",method=RequestMethod.POST)
	@ResponseBody
	public FaceResult getCheckListByStuNumber(Integer id){
		FaceResult result = checkService.getCheckListById(id);
		
		return result;
	}
	
	
	/*
	 * 执行签到
	 * lng 经度
	 * lat 纬度
	 * id 用户标识id
	 */
	@RequestMapping(value="/check",method=RequestMethod.POST)
	@ResponseBody
	public FaceResult check(double lng,double lat,Integer id,String checkInfo,String faceAvatar,String facePhone,String faceGroupUid,String faceOpenId){
		
		FaceResult result = checkService.check(lng, lat, id,checkInfo,faceAvatar,facePhone,faceGroupUid,faceOpenId);
		return result;
		
	}
	/*
	 * 添加公司信息
	 * 其中companyAddress以  经度:纬度的形式赋值 (105.0507400000:29.5891300000)
	 */
	@RequestMapping(value="/stuCompanyInfo",method=RequestMethod.POST)
	@ResponseBody
	public FaceResult insertStudentCompanyInfo(TbStudentCompany company){
		company.setId(null);
		company.setUpdateTime(new Date());
		company.setCreateTime(new Date());
		FaceResult result = checkService.insertStudentCompanyInfo(company);
		return result;
		
	}
	
	/*
	 * 当前签到状态
	 * id 用户id
	 */
	@RequestMapping(value="/checkType",method=RequestMethod.POST)
	@ResponseBody
	public FaceResult getCheckTypeById(Integer id){
		FaceResult result = checkService.getCheckTypeById(id);
		return result;
		
	}
	
	
}

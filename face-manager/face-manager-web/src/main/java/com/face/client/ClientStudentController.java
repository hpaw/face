package com.face.client;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import com.face.common.pojo.FaceResult;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.face.pojo.TbStudent;
import com.face.service.StudentService;

@Controller
@RequestMapping("/client")
public class ClientStudentController {
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public FaceResult login(String stuNumber,String password){
		TbStudent student = studentService.login(stuNumber, password);
		if(student==null){
			return FaceResult.build(400,"登录失败,账号或密码错误!");
		}
		return FaceResult.build(200,"登录成功",student);
	}
	
//		//上传签到图片到服务器
//		@RequestMapping(value="/upload",method=RequestMethod.POST)
//		@ResponseBody
//		public boolean uploadImage(HttpServletRequest request, @RequestParam("id") Integer id,@RequestParam("image") MultipartFile image){
//			String rootPath = request.getSession().getServletContext().getRealPath("/uploadImages/");
//			String imagePath =rootPath;
//			System.out.println(rootPath);
//			String name = image.getOriginalFilename();
//			boolean result=true;
//			 try {
//				
//				 File destFile = new File(imagePath, name);
//		
//				 image.transferTo(destFile);
//				
//				 String url=imagePath+name;
//			 } catch (Exception e) {
//				  e.printStackTrace();
//				  result=false;
//			 }
//			return result;
//			
//		}
}

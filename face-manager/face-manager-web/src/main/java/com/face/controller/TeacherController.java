package com.face.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.common.utils.IDUtils;
import com.face.pojo.TbCollege;
import com.face.pojo.TbTeacher;
import com.face.service.CollegeService;
import com.face.service.TeacherService;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private CollegeService collegeService;
	
	@RequestMapping("/addView")
	@RequiresPermissions("teacher:add")
	public String addView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "teacher-add";
	}
	
	@RequestMapping("/listView")
	@RequiresPermissions("teacher:select")
	public String listView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "teacher-list";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public FaceResult add(TbTeacher teacher,HttpServletRequest request){
		TbTeacher user = (TbTeacher) request.getSession().getAttribute("teacher");
		String salt=IDUtils.getSalt();
		Md5Hash md5=new Md5Hash("123456",salt,1);
		teacher.setPassword(md5.toString());
		teacher.setSalt(salt);
		teacher.setCreatePerson(user.getId());
		FaceResult result = teacherService.add(teacher);
		return result;
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public DataGridResult list(Integer page,Integer rows,String type){
		
		DataGridResult result = teacherService.list(page,rows,type);
		return result;
	}
	
	@RequestMapping("/checkNumber")
	@ResponseBody
	public FaceResult checkNumber(String number){
		FaceResult result = teacherService.checkNumber(number);
		return result;
	}
	
	@RequestMapping("/delete")
	@RequiresPermissions("teacher:delete")
	@ResponseBody
	public FaceResult delete(Integer[] ids){
		FaceResult result = teacherService.delete(ids);
		return result;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public FaceResult update(TbTeacher teacher){
		FaceResult result = teacherService.update(teacher);
		return result;
	}
	
	@RequestMapping("/updateView")
	@RequiresPermissions("teacher:update")
	public String updateView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "teacher-edit";
	}
	
}

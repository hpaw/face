package com.face.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;


import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbCollege;
import com.face.pojo.TbMajor;
import com.face.pojo.TbTeacher;
import com.face.service.CollegeService;
import com.face.service.MajorService;

@Controller
@RequestMapping("/major")
public class MajorController {
	@Autowired
	private MajorService majorService;
	
	@Autowired
	private CollegeService collegeService;
	
	@RequestMapping("/majorView")
	@RequiresPermissions("major:select")
	public String majorView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("cList", list);
		return "majors";
	}
	@RequestMapping("/updateView")
	@RequiresPermissions("major:update")
	public String updateView(){
	
		return "major-edit";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public DataGridResult findMajorsByPage(Integer page,Integer rows,String type){
		
		DataGridResult result = majorService.findMajorsByPage(page, rows,Integer.valueOf(type));
		return result;
	}
	
	@RequestMapping("/addView")
	@RequiresPermissions("major:add")
	public String addView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("colleges", list);
		return "major-add";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public FaceResult add(TbMajor major,HttpSession session){
		TbTeacher user = (TbTeacher) session.getAttribute("teacher");
		major.setCreatePerson(user.getId());
		
		FaceResult result = majorService.add(major);
		return result;
	}
	
	@RequestMapping("/delete")
	@RequiresPermissions("major:delete")
	@ResponseBody
	public FaceResult delete(Integer[] ids){
		FaceResult result = majorService.delete(ids);
		return result;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public FaceResult update(TbMajor major){
		FaceResult result = majorService.update(major);
		return result;
	}
}

package com.face.controller;

import java.util.Date;
import java.util.HashMap;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.FaceResult;
import com.face.common.utils.IDUtils;
import com.face.pojo.TbTeacher;
import com.face.service.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
//	@RequestMapping("/login")
//	public String login(TbTeacher teacher,HttpServletRequest request,Model model,RedirectAttributes redirect,HttpSession session){
//		String code = (String)request.getParameter("code");
//		String vCode =(String)request.getSession().getAttribute("vCode");
//		if(!vCode.equalsIgnoreCase(code)){
//			model.addAttribute("msg", "验证码输入错误！");
//			return "login";
//		}
//		TbTeacher b = loginService.login(teacher);
//		
//		if(b!=null){
//			redirect.addFlashAttribute("teacher", b);
//			session.setAttribute("user", b);
//			return "redirect:main";	
//		}
//		model.addAttribute("msg", "用户名或密码错误！");
//
//		return "login";
//	}
	
	//登陆提交地址，和applicationContext-shiro.xml中配置的loginurl一致
			@RequestMapping("login")
			public String login(HttpServletRequest request)throws Exception{
				
				//如果登陆失败从request中获取认证异常信息，shiroLoginFailure就是shiro异常类的全限定名
				String exceptionClassName = (String) request.getAttribute("shiroLoginFailure");
				//根据shiro返回的异常类路径判断，抛出指定异常信息
				Map<String,String> map=new HashMap<String,String>();
				if(exceptionClassName!=null){
					if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
						//最终会抛给异常处理器
						map.put("error", "账号不存在");
					} else if (IncorrectCredentialsException.class.getName().equals(
							exceptionClassName)) {
						map.put("error", "用户名/密码错误");
					} else if ("verifyCodeError".equals(
							exceptionClassName)) {
						map.put("error", "验证码错误");
					} else {
						map.put("error", "未知错误");
						throw new Exception();//最终在异常处理器生成未知错误
					}
				}
				request.setAttribute("map", map);
				//此方法不处理登陆成功（认证成功），shiro认证成功会自动跳转到上一个请求路径
				//登陆失败还到login页面
				return "login";
			}
	
	
	
	@RequestMapping("/updatePasswordView")
	public String updatePasswordView(){
		
		return "updatePass";
	}
	
	@RequestMapping("/updatePassword")
	@ResponseBody
	public FaceResult updatePass(String password,HttpSession session){
		TbTeacher teacher = (TbTeacher) session.getAttribute("teacher");
		String salt=IDUtils.getSalt();
		Md5Hash md5=new Md5Hash(password,salt,1);
		teacher.setPassword(md5.toString());
		teacher.setSalt(salt);
		teacher.setUpdateTime(new Date());
		FaceResult result = loginService.updatePass(teacher);
		return result;
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session){
			session.invalidate();
			return "redirect:index";	
		}
}

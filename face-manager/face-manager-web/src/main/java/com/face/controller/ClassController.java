package com.face.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbClass;
import com.face.pojo.TbCollege;
import com.face.pojo.TbMajor;
import com.face.pojo.TbTeacher;
import com.face.service.ClassService;
import com.face.service.CollegeService;
import com.face.service.MajorService;

@Controller
@RequestMapping("/class")
public class ClassController {
	
	@Autowired
	private ClassService classService;
	
	@Autowired
	private CollegeService collegeService;
	
	@RequestMapping("/addView")
	@RequiresPermissions("class:add")
	public String addView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("colleges", list);
		return "class-add";
	}
	
	@RequestMapping("/listView")
	@RequiresPermissions("class:select")
	public String listView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "class-list";
	}
	
	@RequestMapping("/updateView")
	@RequiresPermissions("class:update")
	public String updateView(){
		
		return "class-edit";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public FaceResult add(TbClass tbClass,HttpSession session){
		tbClass.setClassName(tbClass.getClassAge()+"级"+tbClass.getClassNumber()+"班");
		TbTeacher user = (TbTeacher) session.getAttribute("teacher");
		tbClass.setCreatePerson(user.getId());
		FaceResult result = classService.add(tbClass);
		return result;
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public DataGridResult list(Integer type){
		  DataGridResult result = classService.findClassByPage(type);
		return result;
	}
	
	@RequestMapping("/delete")
	@RequiresPermissions("class:delete")
	@ResponseBody
	public FaceResult delete(Integer[] ids){
		FaceResult result = classService.delete(ids);
		return result;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public FaceResult update(TbClass tbClass){
		FaceResult result = classService.update(tbClass);
		return result;
	}
	
}

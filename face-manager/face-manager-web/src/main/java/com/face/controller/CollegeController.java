package com.face.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbCollege;
import com.face.pojo.TbTeacher;
import com.face.service.CollegeService;

@Controller
@RequestMapping("/college")
public class CollegeController {
	@Autowired
	private CollegeService collegeService;
	
	@RequestMapping(value="/collegeView")
	@RequiresPermissions("college:select")
	public String collegeView(){
		return "colleges";
	}
	
	@RequestMapping(value="/add")
	@RequiresPermissions("college:add")
	public String add(){
		return "college-add";
	}
	
	@RequestMapping(value="/updateView")
	@RequiresPermissions("college:update")
	public String updateView(){
		return "college-edit";
	}
	@RequestMapping("/list")
	@ResponseBody
	public DataGridResult findCollegesByPage(Integer page,Integer rows){
		DataGridResult result = collegeService.findCollegesByPage(page, rows);
		return result;
	}
	
	@RequestMapping("/delete")
	@RequiresPermissions("college:delete")
	@ResponseBody
	public FaceResult deleteByIds(Integer[] ids){
		FaceResult result = collegeService.deleteByIds(ids);
		return result;
	}
	
	@RequestMapping("/addCollege")
	@ResponseBody
	public FaceResult addCollege(TbCollege college,HttpSession session){
		TbTeacher user = (TbTeacher) session.getAttribute("teacher");
		college.setCreatePerson(user.getId());
		FaceResult result = collegeService.add(college);
		return result;
	}
	
	
	@RequestMapping("/update")
	@ResponseBody
	public FaceResult update(TbCollege college){
		FaceResult result = collegeService.update(college);
		return result;
	}
	
	
}

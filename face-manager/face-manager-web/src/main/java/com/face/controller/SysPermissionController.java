package com.face.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.FaceResult;
import com.face.pojo.MenuAndPermission;
import com.face.pojo.SysPermission;
import com.face.pojo.TeacherPermissions;
import com.face.service.SysPermissionService;

@Controller
@RequestMapping("/permission")
public class SysPermissionController {
	
	@Autowired
	private SysPermissionService permissionService;
	

	
	@RequestMapping("/permissionAndMenu")
	public String getAllPermissionAndMenu(Model model){
		model.addAttribute("list", getPermissions());
		return "authority-list";
	}
	

	@RequestMapping("/addPermissionView/{id}")
	@RequiresPermissions("permission:add")
	public String addPermissionView(Model model,@PathVariable Integer id){
		
		model.addAttribute("list", getPermissions());
		model.addAttribute("id", id);
		
		return "permission";
	}
	
	public List<MenuAndPermission> getPermissions(){
		List<MenuAndPermission> list = permissionService.getPermissionAndMenu();
		return list;
	}
	
	@RequestMapping("/addPermission")
	@ResponseBody
	public FaceResult addPermission(Integer teacherId,Long[] permissions){
		
		FaceResult result = permissionService.insertPermissions(teacherId, permissions);
		return result;
	}
	
	@RequestMapping("/editPermissionView/{id}")
	@RequiresPermissions("permission:update")
	public String editPermissionView(Model model,@PathVariable Integer id){
		
		model.addAttribute("list", getPermissions());
		model.addAttribute("id", id);
		List<SysPermission> list = permissionService.getPermissionsByTeacherId(id);
		model.addAttribute("oldPermissions",list);
		return "editPermission";
	}
	
	@RequestMapping("/editPermission")
	@ResponseBody
	public FaceResult editPermission(Integer teacherId,Long[] permissions){
		FaceResult result=null;
		if(permissions==null||permissions.length==0){
			result=FaceResult.build(400, "修改的权限不能为空");
		}else{
			result = permissionService.updatePermissions(teacherId, permissions);

		}
		return result;
	}
}

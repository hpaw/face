package com.face.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.DataGridResult;
import com.face.pojo.Query;
import com.face.pojo.TbCollege;
import com.face.service.CheckService;
import com.face.service.CollegeService;

@Controller
@RequestMapping("/check")
public class CheckController {
	
	@Autowired
	private CollegeService collegeService;
	
	@Autowired
	private CheckService checkService;
	
	@RequestMapping("/query")
	public String query(Model model){
		
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "collegeQuery";
	}
	
	@RequestMapping(value="/list",method=RequestMethod.POST)
	@ResponseBody
	public DataGridResult queryView(Model model,Query query,Integer page,Integer rows){
		
		System.out.println("q="+query);
		DataGridResult result = checkService.list(page, rows,query);
		System.out.println(result);
		
		return result;
	}
	
}

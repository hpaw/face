package com.face.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.face.common.pojo.DataGridResult;
import com.face.common.pojo.FaceResult;
import com.face.pojo.TbClass;
import com.face.pojo.TbCollege;
import com.face.pojo.TbMajor;
import com.face.pojo.TbStudent;
import com.face.pojo.TbTeacher;
import com.face.service.CollegeService;
import com.face.service.MajorService;
import com.face.service.StudentService;
import com.mysql.fabric.xmlrpc.base.Data;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private MajorService majorService;

	
	@Autowired
	private CollegeService collegeService;
	
	@RequestMapping("/addView")
	@RequiresPermissions("student:add")
	public String addView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "student-add";
	}
	
	@RequestMapping("/updateView")
	@RequiresPermissions("student:update")
	public String updateView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "student-edit";
	}
	
	@RequestMapping("/listView")
	@RequiresPermissions("student:select")
	public String listView(Model model){
		List<TbCollege> list = collegeService.findAll();
		model.addAttribute("list", list);
		return "student-list";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public FaceResult add(TbStudent student,HttpSession session){
		
		TbTeacher user = (TbTeacher) session.getAttribute("teacher");
		student.setCreatePerson(user.getId());
		
		FaceResult result = studentService.add(student);
		return result;
	}
	
	@RequestMapping("/delete")
	@RequiresPermissions("student:delete")
	@ResponseBody
	public FaceResult delete(Integer[] ids){
		FaceResult result = studentService.delete(ids);
		return result;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public FaceResult update(TbStudent student){
		FaceResult result = studentService.update(student);
		return result;
	}
	@RequestMapping("/list")
	@ResponseBody
	public DataGridResult list(Integer page,Integer rows,String type){
		DataGridResult result = studentService.list(page, rows,type);
		return result;
	}
	
	@RequestMapping(value="/data")
	@ResponseBody
	public List<TbMajor> data(String name){
		List<TbMajor> list = majorService.findByCollegeName(name);
		
		return list;
	}
	
	@RequestMapping(value="/classInfo")
	@ResponseBody
	public List<TbClass> classInfo(String name){
		
		List<TbClass> list = studentService.ClassInfo(name);
		return list;
	}
	
	@RequestMapping(value="/checkStuNumber")
	@ResponseBody
	public FaceResult checkStuNumber(String number){
		FaceResult result = studentService.checkStuNumber(number);
		return result;
	}
}

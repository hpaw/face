package com.face.controller;

import java.awt.image.BufferedImage;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.face.common.utils.VerifyCode;
@Controller
public class Verification {
	
	@RequestMapping("/verifyCode")
	public void verifyCode(HttpServletRequest request, HttpServletResponse response){
		VerifyCode vc = new VerifyCode();
		BufferedImage image = vc.getImage();
		try {
			VerifyCode.output(image, response.getOutputStream());
			
			request.getSession().setAttribute("vCode", vc.getText());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}

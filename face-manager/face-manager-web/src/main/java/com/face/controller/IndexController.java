package com.face.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.face.pojo.TbTeacher;

@Controller
public class IndexController {

	@RequestMapping("/index")
	public String index() {

		return "login";
	}

	// 系统首页
	@RequestMapping("/first")
	public String first(HttpSession session) throws Exception {

		// 从shiro的session中取activeUser
		Subject subject = SecurityUtils.getSubject();
		// 取身份信息
		TbTeacher teacher = (TbTeacher) subject.getPrincipal();
		session.setAttribute("teacher", teacher);

		return "main";
	}

}

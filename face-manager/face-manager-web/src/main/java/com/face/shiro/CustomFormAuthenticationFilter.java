package com.face.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

public class CustomFormAuthenticationFilter extends FormAuthenticationFilter{
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
			HttpServletRequest req=(HttpServletRequest) request;
			String code = (String) req.getParameter("code");
			String vCode = (String) req.getSession().getAttribute("vCode");
			System.out.println(code+":"+vCode);
			if(code==null||!code.equalsIgnoreCase(vCode)){
				req.setAttribute("shiroLoginFailure", "verifyCodeError");
				return true;
			}
		return super.onAccessDenied(request, response);
	}
	
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
			ServletResponse response) throws Exception {
		WebUtils.issueRedirect(request, response, getSuccessUrl());
		return false;
	}
}

package com.face.mapper;

import java.util.List;

import com.face.pojo.Query;
import com.face.pojo.QueryResult;

public interface CheckQueryMapper {
	public List<QueryResult> query(Query query);
}

package com.face.pojo;

public class TeacherPermissions {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column teacher_permissions.id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column teacher_permissions.teacher_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    private Integer teacherId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column teacher_permissions.sys_permission_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    private Long sysPermissionId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column teacher_permissions.id
     *
     * @return the value of teacher_permissions.id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column teacher_permissions.id
     *
     * @param id the value for teacher_permissions.id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column teacher_permissions.teacher_id
     *
     * @return the value of teacher_permissions.teacher_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column teacher_permissions.teacher_id
     *
     * @param teacherId the value for teacher_permissions.teacher_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column teacher_permissions.sys_permission_id
     *
     * @return the value of teacher_permissions.sys_permission_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public Long getSysPermissionId() {
        return sysPermissionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column teacher_permissions.sys_permission_id
     *
     * @param sysPermissionId the value for teacher_permissions.sys_permission_id
     *
     * @mbggenerated Thu Apr 13 16:48:22 CST 2017
     */
    public void setSysPermissionId(Long sysPermissionId) {
        this.sysPermissionId = sysPermissionId;
    }
}
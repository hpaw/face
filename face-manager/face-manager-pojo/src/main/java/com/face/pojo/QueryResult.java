package com.face.pojo;

import java.util.Date;

public class QueryResult {
	private int id;
	private String studentName;
	private String collegeName;
	private String majorName;
	private String className;
	private String checkType;
	private Date checkTime;
	private String checkAddress;
	private int checkYear;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getCheckType() {
		return checkType;
	}
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	public Date getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	public String getCheckAddress() {
		return checkAddress;
	}
	public void setCheckAddress(String checkAddress) {
		this.checkAddress = checkAddress;
	}
	public int getCheckYear() {
		return checkYear;
	}
	public void setCheckYear(int checkYear) {
		this.checkYear = checkYear;
	}
	@Override
	public String toString() {
		return "QueryResult [id=" + id + ", studentName=" + studentName + ", collegeName=" + collegeName
				+ ", majorName=" + majorName + ", className=" + className + ", checkType=" + checkType + ", checkTime="
				+ checkTime + ", checkAddress=" + checkAddress + ", checkYear=" + checkYear + "]";
	}
	
	
	
}

package com.face.pojo;

import java.io.Serializable;

public class Query implements Serializable{
	private Integer year;
	private String collegeName;
	private String majorName;
	private String className;
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	@Override
	public String toString() {
		return "Query [year=" + year + ", collegeName=" + collegeName + ", majorName=" + majorName + ", className="
				+ className + "]";
	}




}

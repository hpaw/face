package com.face.pojo;

import java.io.Serializable;
import java.util.List;

public class TeacherInfo implements Serializable{
	TbTeacher teacher;
	List<SysPermission> list;
	public TbTeacher getTeacher() {
		return teacher;
	}
	public void setTeacher(TbTeacher teacher) {
		this.teacher = teacher;
	}
	public List<SysPermission> getList() {
		return list;
	}
	public void setList(List<SysPermission> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "TeacherInfo [teacher=" + teacher + ", list=" + list + "]";
	}
	
	
}

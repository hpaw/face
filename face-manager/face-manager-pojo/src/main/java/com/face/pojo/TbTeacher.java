package com.face.pojo;

import java.util.Date;

public class TbTeacher {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.teacher_number
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private String teacherNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.teacher_name
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private String teacherName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.password
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private String password;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.teacher_type
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private String teacherType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.college_id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private Integer collegeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.create_person
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private Integer createPerson;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.create_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.update_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private Date updateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_teacher.salt
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    private String salt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.id
     *
     * @return the value of tb_teacher.id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.id
     *
     * @param id the value for tb_teacher.id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.teacher_number
     *
     * @return the value of tb_teacher.teacher_number
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public String getTeacherNumber() {
        return teacherNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.teacher_number
     *
     * @param teacherNumber the value for tb_teacher.teacher_number
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setTeacherNumber(String teacherNumber) {
        this.teacherNumber = teacherNumber == null ? null : teacherNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.teacher_name
     *
     * @return the value of tb_teacher.teacher_name
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.teacher_name
     *
     * @param teacherName the value for tb_teacher.teacher_name
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.password
     *
     * @return the value of tb_teacher.password
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.password
     *
     * @param password the value for tb_teacher.password
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.teacher_type
     *
     * @return the value of tb_teacher.teacher_type
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public String getTeacherType() {
        return teacherType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.teacher_type
     *
     * @param teacherType the value for tb_teacher.teacher_type
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType == null ? null : teacherType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.college_id
     *
     * @return the value of tb_teacher.college_id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public Integer getCollegeId() {
        return collegeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.college_id
     *
     * @param collegeId the value for tb_teacher.college_id
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.create_person
     *
     * @return the value of tb_teacher.create_person
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public Integer getCreatePerson() {
        return createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.create_person
     *
     * @param createPerson the value for tb_teacher.create_person
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.create_time
     *
     * @return the value of tb_teacher.create_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.create_time
     *
     * @param createTime the value for tb_teacher.create_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.update_time
     *
     * @return the value of tb_teacher.update_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.update_time
     *
     * @param updateTime the value for tb_teacher.update_time
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_teacher.salt
     *
     * @return the value of tb_teacher.salt
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public String getSalt() {
        return salt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_teacher.salt
     *
     * @param salt the value for tb_teacher.salt
     *
     * @mbggenerated Mon Apr 17 15:55:09 CST 2017
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }
}
package com.face.pojo;

import java.util.Date;

public class TbClass {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.class_number
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Integer classNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.class_name
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private String className;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.class_age
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private String classAge;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.major_id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Integer majorId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.create_person
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Integer createPerson;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.create_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tb_class.update_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.id
     *
     * @return the value of tb_class.id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.id
     *
     * @param id the value for tb_class.id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.class_number
     *
     * @return the value of tb_class.class_number
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Integer getClassNumber() {
        return classNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.class_number
     *
     * @param classNumber the value for tb_class.class_number
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setClassNumber(Integer classNumber) {
        this.classNumber = classNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.class_name
     *
     * @return the value of tb_class.class_name
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public String getClassName() {
        return className;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.class_name
     *
     * @param className the value for tb_class.class_name
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.class_age
     *
     * @return the value of tb_class.class_age
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public String getClassAge() {
        return classAge;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.class_age
     *
     * @param classAge the value for tb_class.class_age
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setClassAge(String classAge) {
        this.classAge = classAge == null ? null : classAge.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.major_id
     *
     * @return the value of tb_class.major_id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Integer getMajorId() {
        return majorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.major_id
     *
     * @param majorId the value for tb_class.major_id
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setMajorId(Integer majorId) {
        this.majorId = majorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.create_person
     *
     * @return the value of tb_class.create_person
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Integer getCreatePerson() {
        return createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.create_person
     *
     * @param createPerson the value for tb_class.create_person
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.create_time
     *
     * @return the value of tb_class.create_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.create_time
     *
     * @param createTime the value for tb_class.create_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tb_class.update_time
     *
     * @return the value of tb_class.update_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tb_class.update_time
     *
     * @param updateTime the value for tb_class.update_time
     *
     * @mbggenerated Wed Mar 08 21:10:27 CST 2017
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
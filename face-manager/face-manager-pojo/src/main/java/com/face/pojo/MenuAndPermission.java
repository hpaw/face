package com.face.pojo;

import java.util.List;

public class MenuAndPermission {
	private SysPermission menu;
	private List<SysPermission> permissions;
	@Override
	public String toString() {
		return "MenuAndPermission [menu=" + menu + ", permissions=" + permissions + "]";
	}
	public SysPermission getMenu() {
		return menu;
	}
	public void setMenu(SysPermission menu) {
		this.menu = menu;
	}
	public List<SysPermission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<SysPermission> permissions) {
		this.permissions = permissions;
	}
	
	
}

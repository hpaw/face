/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.36 : Database - face
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`face` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `face`;

/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

CREATE TABLE `sys_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `percode` varchar(50) DEFAULT NULL,
  `parentid` bigint(20) DEFAULT NULL,
  `parentids` varchar(100) DEFAULT NULL,
  `sortstring` int(11) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission` */

insert  into `sys_permission`(`id`,`name`,`type`,`url`,`percode`,`parentid`,`parentids`,`sortstring`,`available`) values (1,'院系管理','menu',NULL,NULL,0,'0/',1,1),(2,'删除院系','permission',NULL,'college:delete',1,'0/1/',NULL,1),(3,'修改院系','permission',NULL,'college:update',1,'0/1/',NULL,1),(4,'查询院系','permission',NULL,'college:select',1,'0/1/',NULL,1),(5,'增加院系','permission',NULL,'college:add',1,'0/1/',NULL,1),(6,'专业管理','menu',NULL,NULL,0,'0/',2,1),(7,'删除专业','permission',NULL,'major:delete',6,'0/6/',NULL,1),(8,'修改专业','permission',NULL,'major:update',6,'0/6/',NULL,1),(9,'查询专业','permission',NULL,'major:select',6,'0/6/',NULL,1),(10,'增加专业','permission',NULL,'major:add',6,'0/6/',NULL,1),(11,'班级管理','menu',NULL,NULL,0,'0/',3,1),(12,'删除班级','permission',NULL,'class:delete',11,'0/11/',NULL,1),(13,'修改班级','permission',NULL,'class:update',11,'0/11/',NULL,1),(14,'查询班级','permission',NULL,'class:select',11,'0/11/',NULL,1),(15,'添加班级','permission',NULL,'class:add',11,'0/11/',NULL,1),(16,'教师管理','menu',NULL,NULL,0,'0/',4,1),(17,'删除教师','permission',NULL,'teacher:delete',16,'0/16/',NULL,1),(18,'修改教师','permission',NULL,'teacher:update',16,'0/16/',NULL,1),(19,'查询教师','permission',NULL,'teacher:select',16,'0/16/',NULL,1),(20,'添加教师','permission',NULL,'teacher:add',16,'0/16/',NULL,1),(21,'学生管理','menu',NULL,NULL,0,'0/',5,1),(22,'添加学生','permission',NULL,'student:add',21,'0/21/',NULL,1),(23,'修改学生','permission',NULL,'student:update',21,'0/21/',NULL,1),(24,'删除学生','permission',NULL,'student:delete',21,'0/21/',NULL,1),(25,'查询学生','permission',NULL,'student:select',21,'0/21/',NULL,1),(26,'权限管理','menu',NULL,NULL,0,'0/',6,1),(27,'修改权限','permission',NULL,'permission:update',26,'0/26/',NULL,1),(28,'分配权限','permission',NULL,'permission:add',26,'0/26/',NULL,NULL);

/*Table structure for table `tb_check` */

DROP TABLE IF EXISTS `tb_check`;

CREATE TABLE `tb_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `check_type` varchar(20) DEFAULT NULL,
  `check_time` datetime DEFAULT NULL,
  `check_address` varchar(200) DEFAULT NULL,
  `check_year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `tb_check` */

insert  into `tb_check`(`id`,`student_id`,`check_type`,`check_time`,`check_address`,`check_year`) values (12,21,'已签到','2017-05-24 11:35:41','105.04919:29.59002',2017),(13,21,'已签到','2017-05-24 11:35:41','105.04919:29.59002',2017),(14,21,'已签到','2017-05-24 11:35:41','105.04919:29.59002',2017),(15,22,'已签到','2017-05-25 11:35:41','105.04919:29.59002',2017);

/*Table structure for table `tb_class` */

DROP TABLE IF EXISTS `tb_class`;

CREATE TABLE `tb_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_number` int(11) DEFAULT NULL,
  `class_name` varchar(20) DEFAULT NULL,
  `class_age` varchar(20) DEFAULT NULL,
  `major_id` int(11) DEFAULT NULL,
  `create_person` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_class` */

/*Table structure for table `tb_college` */

DROP TABLE IF EXISTS `tb_college`;

CREATE TABLE `tb_college` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_name` varchar(40) DEFAULT NULL,
  `create_person` int(20) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `tb_college` */

insert  into `tb_college`(`id`,`college_name`,`create_person`,`create_time`,`update_time`) values (2,'外语学院',1,'2017-03-09','2017-03-09'),(4,'体育学院',1,'2017-03-09','2017-03-09'),(7,'音乐学院',NULL,NULL,'2017-03-09'),(8,'范长江新闻学院',1,'2017-03-10','2017-03-10'),(9,'数信与数学科学学院',1,'2017-03-10','2017-03-10'),(10,'建筑工程学院',1,'2017-03-10','2017-03-10'),(11,'化学化工学院',1,'2017-03-10','2017-03-10'),(12,'政治与公共管理学院',1,'2017-03-10','2017-03-10'),(13,'生命科学学院',1,'2017-03-10','2017-03-10'),(14,'教育科学学院',1,'2017-03-10','2017-03-10'),(15,'经济与管理学院',1,'2017-03-10','2017-03-10'),(16,'马克思主义学院',1,'2017-03-10','2017-03-10'),(17,'地理与资源科学学院',1,'2017-03-10','2017-03-10'),(18,'文学院',1,'2017-03-10','2017-03-10');

/*Table structure for table `tb_major` */

DROP TABLE IF EXISTS `tb_major`;

CREATE TABLE `tb_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `major_name` varchar(40) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `create_person` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_major` */

/*Table structure for table `tb_student` */

DROP TABLE IF EXISTS `tb_student`;

CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_number` varchar(20) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `student_name` varchar(40) DEFAULT NULL,
  `college_name` varchar(40) DEFAULT NULL,
  `class_name` varchar(40) DEFAULT NULL,
  `major_name` varchar(40) DEFAULT NULL,
  `student_images` varchar(100) DEFAULT NULL,
  `create_person` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `tb_student` */

insert  into `tb_student`(`id`,`student_number`,`password`,`student_name`,`college_name`,`class_name`,`major_name`,`student_images`,`create_person`,`create_time`,`update_time`) values (21,'20141042014','96e79218965eb72c92a549dd5a330112','李成林','计算机科学学院','2014级1班','软件工程',NULL,1,'2017-03-16','2017-03-16'),(22,'20141042095','96e79218965eb72c92a549dd5a330112','彭和平','计算机科学学院','2014级3班','互联网工程',NULL,1,'2017-03-16','2017-03-16');

/*Table structure for table `tb_student_company` */

DROP TABLE IF EXISTS `tb_student_company`;

CREATE TABLE `tb_student_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) DEFAULT NULL,
  `company_city` varchar(20) DEFAULT NULL,
  `company_address` varchar(200) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  `company_info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tb_student_company` */

insert  into `tb_student_company`(`id`,`company_name`,`company_city`,`company_address`,`student_id`,`create_time`,`update_time`,`company_info`) values (1,'内江师范学院','内江','105.0507400000:29.5891300000',21,NULL,NULL,NULL);

/*Table structure for table `tb_teacher` */

DROP TABLE IF EXISTS `tb_teacher`;

CREATE TABLE `tb_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_number` varchar(20) DEFAULT NULL,
  `teacher_name` varchar(40) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `teacher_type` varchar(20) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `create_person` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  `salt` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_teacher` */

insert  into `tb_teacher`(`id`,`teacher_number`,`teacher_name`,`password`,`teacher_type`,`college_id`,`create_person`,`create_time`,`update_time`,`salt`) values (1,'1008600001','彭和平','9134105c15ecc7f3adf0119e4883ec20','普通教师',1,NULL,NULL,'2017-05-26','089aa'),(3,'1008600002','张三','686a286a22cb22ec6ef1447c70f3d1e1','普通教师',2,1,'2017-05-12','2017-05-12','ce465');

/*Table structure for table `teacher_permissions` */

DROP TABLE IF EXISTS `teacher_permissions`;

CREATE TABLE `teacher_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `sys_permission_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `teacher_permissions` */

insert  into `teacher_permissions`(`id`,`teacher_id`,`sys_permission_id`) values (1,1,2),(2,1,3),(3,1,4),(4,1,5),(5,1,7),(6,1,8),(7,1,9),(8,1,10),(9,6,4),(10,6,9),(11,7,4),(12,7,9),(13,1,10),(15,1,12),(16,1,13),(17,1,14),(18,1,15),(20,1,17),(21,1,18),(22,1,19),(23,1,20),(25,1,22),(26,1,23),(27,1,24),(28,1,25),(29,1,27),(30,2,4),(31,2,9),(32,2,14),(33,2,19),(34,1,28),(51,3,2),(52,3,3),(53,3,5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
